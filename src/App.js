import React, { Component } from "react";
import axios from "axios";

import "./App.css";

class App extends Component {
  state = {
    tasks: [],
    name: "",
    lastName: "",
    flag: false
  };
  getData = () => {
    axios
      .get("http://localhost:3004/posts")
      .then(response => {
        // console.log(response);
        // handle success
        // console.log(response.data);
        this.setState({
          tasks: response.data,
          value: ""
        });
        console.log(this.state.tasks);
      })

      .catch(function(error) {
        // handle error
        console.log(error);
      })
      .finally(function() {
        // always executed
      });

    if (this.state.tasks.length === 0) {
      console.log("taski 0");
    } else {
      console.log("taski >0");
    }
  };
  componentDidMount() {
    this.getData();
  }
  postData = () => {
    if (this.state.name) {
      let name = this.state.name;
      // let surname = this.state.lastName;
      const date = new Date();
      axios
        .post("http://localhost:3004/posts", {
          title: name,
          // author: surname,
          date: `Data dodania: ${
            date.getDate() < 10 ? "0" + date.getDate() : date.getDate()
          }/${
            date.getMonth() < 10 ? "0" + date.getMonth() : date.getMonth()
          }/${date.getFullYear()}`
        })
        .then(function(response) {
          console.log(response);
        })
        .catch(function(error) {
          console.log(error);
        });
      this.setState({
        name: "",
        lastName: ""
      });
      setTimeout(this.getData, 100);
    } else alert("wypełnij pole z zadaniem!");
  };
  handleChange = e => {
    if (e.target.id === "name") {
      this.setState({
        name: e.target.value
      });
    } else
      this.setState({
        lastName: e.target.value
      });
  };
  handleRemove = id => {
    console.log(id);
    axios
      .delete(`http://localhost:3004/posts/${id}`)
      .then(response => {
        console.log(response);
      })
      .catch(function(error) {
        console.log(error);
      });
    setTimeout(this.getData, 100);
  };
  render() {
    const array = this.state.tasks.map(task => (
      <li key={task.id}>
        <span className="task">{task.title}</span> |{" "}
        <span className="date">{task.date}</span>
        <button id={task.id} onClick={() => this.handleRemove(task.id)}>
          X
        </button>
      </li>
    ));
    return (
      <>
        <div className="app">
          <h1>Dodaj Zadanie do wykonania!</h1>
          <input
            type="text"
            value={this.state.name}
            id="name"
            onChange={this.handleChange}
            placeholder="Dodaj zadanie..."
          />
          {/* <input
          type="text"
          value={this.state.lastName}
          id="lastName"
          onChange={this.handleChange}
          placeholder="Wpisz tutaj..."
        /> */}
          <button onClick={this.postData}>send</button>
          {/* <button onClick={this.getData}>GET DATA</button> */}
          <ul>{this.state.tasks ? array : null}</ul>
        </div>
      </>
    );
  }
}

export default App;
